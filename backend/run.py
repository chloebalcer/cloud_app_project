from flask import Flask
from flask_cors import CORS, cross_origin

app = Flask(__name__)
from bson import json_util
from flask_pymongo import PyMongo, MongoClient

from sshtunnel import SSHTunnelForwarder

MONGO_HOST = "devicimongodb105.westeurope.cloudapp.azure.com"
MONGO_USER = "administrateur"
MONGO_PASS = "fcwP6h3H"
MONGO_DB = "movies"

server = SSHTunnelForwarder(
    MONGO_HOST,
    ssh_username=MONGO_USER,
    ssh_password=MONGO_PASS,
    remote_bind_address=(MONGO_DB, 22)
)

MONGO_URI = "mongodb://administrateur:fcwP6h3H@devicimongodb002.westeurope.cloudapp.azure.com:30000/movies?ssh=true"
app.config.from_pyfile('config.py')
cors = CORS(app)
app.config['CORS_HEADERS'] = 'Content-Type'

with app.app_context():
    mongo = PyMongo(app)
    db = mongo.db


@app.route("/movies")
def hello():
    movies = db['movies_credits1'].find().limit(2)
    json_movies = json_util.dumps(movies)
    return json_movies


@app.route("/")
def welcomeapp():
    try:
        server.start()
        client = MongoClient(MONGO_HOST, 30000)
        return json_util.dumps(client.server_info())
    except Exception as e:
        print(e)
        return e


@app.route("/requete1")
def requete1():
    sport_american_movies = db['movies'].find({
        "production_countries.name": "United States of America",
        "overview": {
            "$regex": "sport",
            "$options": "i"
        }
    },
        {
            "original_title": 1
        })
    json_movies = json_util.dumps(sport_american_movies)
    return json_movies


@app.route("/requete2")
def requete2():
    movies_desc_vote_average = db['movies_credits1'].aggregate([
        {
            "$project": {
                "genres.name": 1,
                "vote_average": 1
            }

        },
        {
            "$sort": {"vote_average": -1}
        },
        {"$limit": 1}
    ])
    json_movies = json_util.dumps(movies_desc_vote_average)
    return json_movies


@app.route("/requete3")
def requete3():
    matt_damon_movie = db['movies_credits1'].aggregate([{
        "$unwind": "$cast"
    },
        {
            "$match": {"cast.name": "Matt Damon"}
        },

        {"$sort": {"Budget": -1}
         },
        {
            "$project": {"film": "$original_title"}
        },
        {"$limit": 10
         }
    ])
    json_movies = json_util.dumps(matt_damon_movie)
    return json_movies


@app.route("/requete4")
def requete4():
    jim_carrey_movies = db['movies_credits1'].aggregate([
        {"$addFields":
             {"convertedvote_avg": {"$toDouble": "$vote_average"}}
         },

        {

            "$unwind": "$cast"
        },

        {

            "$match": {

                "cast.name": "Jim Carrey",

                "convertedvote_avg": {

                    "$gt": 6.0,

                    "$lt": 10.0
                }
            }
        },
        {

            "$count": "movies_nb"
        }
    ])
    json_movies = json_util.dumps(jim_carrey_movies)
    return json_movies


@app.route("/requete5")
def requete5():
    movies = db['movies_credits1'].aggregate([
        {
            "$unwind": "$cast"
        },
        {
            "$match": {
                "cast.gender": 1
            }
        },
        {
            "$group": {

                "_id": "$cast.character",
                "count": {
                    "$sum": 1
                }
            }
        },
        {
            "$sort": {

                "count": -1
            }
        },
        {

            "$limit": 3
        }
    ])
    json_movies = json_util.dumps(movies)
    return json_movies


@app.route("/requete6")
def requete6():
    movies = db['movies_credits1'].aggregate([
        {"$addFields":
             {"convertedvote_count": {"$toInt": "$vote_count"}},
         },
        {"$addFields":
             {"convertedrelease_date": {"$toDate": "$release_date"}},
         },
        {"$addFields":
             {"convertedrelease_date_2": {"year": {"$year": ["$convertedrelease_date"]},
                                          "month": {"$month": ["$convertedrelease_date"]},
                                          "day": {"$dayOfMonth": ["$convertedrelease_date"]}}},
         },
        {
            "$match": {
                "convertedvote_count": {
                    "$gt": 250}}},
        {
            "$project": {
                "belongs_to_collection.name": 1,
                "vote_average": 1,
                "convertedvote_count": 1,
                "convertedrelease_date": 1,
                "convertedrelease_date_2": 1,
                "decade": {
                    "$subtract": [
                        "$convertedrelease_date_2.year",
                        {
                            "$mod": [
                                "$convertedrelease_date_2.year",
                                10]
                        }
                    ]
                }

            }},

        {"$sort": {
            "vote_average": -1},
        },
        {
            "$limit": 1
        }
    ])
    json_movies = json_util.dumps(movies)
    return json_movies


@app.route("/requete7")
def requete7():
    movies = db['keyword_genre'].aggregate([
        {"$unwind": "$genre"},
        {"$unwind": "$keyword"},
        {"$group": {"_id": {"keyword": "$keyword",
                            "genre": "$genre"},
                    "count": {"$sum": 1}
                    }
         },
        {"$sort": {"count": -1}}
    ])
    json_movies = json_util.dumps(movies)
    return json_movies


@app.route("/requete8")
def requete8():
    movies = db['movies_credits1'].aggregate([{

        "$match": {

            "genres": {
                "$elemMatch": {
                    "name": "Drama"
                }
            }
        }
    },

        {

            "$unwind": "$cast"
        },

        {

            "$group": {

                "_id": {

                    "cast_name": "$cast.name"
                },

                "total_cast_vote_average": {

                    "$avg": "$vote_average"
                }
            }
        },
        {

            "$project": {

                "cast_name": 1,

                "total_cast_vote_average": 1
            }
        },
        {

            "$sort": {

                "total_cast_vote_average": -1
            }
        },

        {
            "$limit": 10
        }])
    json_movies = json_util.dumps(movies)
    return json_movies


if __name__ == "__main__":
    app.run()
