import React, { Component } from "react";
import { BrowserRouter, Route, Switch } from "react-router-dom";
import HomePage from "./components/HomePage/HomePage";
import Movies from "./components/Movies";

import './App.css'
import AmateurQueries from "./components/Queries/AmateurQueries";
import DataAnalystQueries from "./components/Queries/DataAnalystQueries";

class App extends Component {
  render() {
    return (
      <div>
        <BrowserRouter>
          <Switch>
            <Route exact path="/" component={HomePage} />
            <Route path="/movies" component={Movies}/>
            <Route path="/amateur-queries" component={AmateurQueries}/>
            <Route path="/data-analyst-queries" component={DataAnalystQueries}/>
          </Switch>
        </BrowserRouter>
      </div>
    );
  }
}

export default App;

