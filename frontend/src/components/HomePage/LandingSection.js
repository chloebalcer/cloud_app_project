import React, {Component} from 'react';
import './LandingSection.css';
import {Modal, Button} from 'react-bootstrap';
import landingImage from '../../assets/images/landing-img.png';

class LandingSection extends Component {
    constructor() {
        super();
        this.state={
            show:false
        }
    }
    handleModal(){
        this.setState({show:!this.state.show})
    }
    render(){
        return (
      <div className="container-fluid">
          <div className="left-section">
            <h3 className="light-font"> For expert analysts and curious movies amateurs</h3>
              <h1>
                  Explore Metadata on over 45,000 movies with Simple Requests !
              </h1>
              <div>
                  <button type="button" className="btn btn-primary btn-lg" onClick={()=>this.handleModal()}>Explore</button>
                  <Modal show={this.state.show}>
                      <Modal.Header> Choose your identity</Modal.Header>
                      <Modal.Body className='modal-body'>
                          <div className="block-div">
                              <a href="/amateur-queries">
                                   <Button className='redirect-button' >
                              Movie Amateur
                          </Button>
                              </a>
                          <br/><a href="/data-analyst-queries">
                              <Button className='redirect-button' >
                              Data Analyst
                          </Button>
                          </a>
                          </div>
                      </Modal.Body>
                      <Modal.Footer>
                          <Button className={'style-button'} onClick={()=>this.handleModal()}>
                              Close
                          </Button>
                      </Modal.Footer>
                  </Modal>
              </div>
          </div>
          <div className="right-section">
              <div className="img-container container">
            <img className="landing-image" alt="finance-platform-illustration" src={landingImage} />
          </div>
              </div>
      </div>
  )
    }

}

export default LandingSection;
