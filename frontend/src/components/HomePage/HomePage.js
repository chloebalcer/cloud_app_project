import React, { Component } from "react";
import HomeNavbar from '../HomeNavbar/HomeNavbar'
import LandingSection from "./LandingSection";
import {Container} from "react-bootstrap";

class HomePage extends Component {
  render() {
    return (
      <div className="window-size">
        <HomeNavbar />
        <LandingSection/>
      </div>
    );
  }
}

export default HomePage;

