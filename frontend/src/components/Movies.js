import React from 'react';

import axios from 'axios';

export default class Movies extends React.Component {
  state = {
    movies: []
  }

  componentDidMount() {
    axios.get("/movies")
      .then(res => {
        const movies = res.data;
        console.log(movies);
        this.setState({ movies });
      })
  }

  render() {
    return (
      <ul>
        { this.state.movies.map((movie, i) => <li key={i}>{movie.id}</li>)}
      </ul>
    )
  }
}