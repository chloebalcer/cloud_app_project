import React from 'react';
import './LandingSection.css';

import landingImage from '../../assets/images/landing-img.png';


const LandingSection = () => {
  return (
      <div className="container-fluid">
          <div className="left-section">
            <h3 className="light-font"> For expert analysts and curious movies amateurs</h3>
              <h1>
                  Explore Metadata on over 45,000 movies with Simple Requests !
              </h1>
              <div>
                  <button type="button" class="btn btn-primary btn-lg">Explore</button>
              </div>
          </div>
          <div className="right-section">
              <div className="img-container container">
            <img className="landing-image" alt="finance-platform-illustration" src={landingImage} />
          </div>
              </div>
      </div>
  )
}

export default LandingSection;
