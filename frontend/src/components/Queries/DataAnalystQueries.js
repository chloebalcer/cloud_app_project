import React, {Component} from 'react';
import './DataAnalystQueries.css'
import {Link} from "react-router-dom";
import {Navbar, Accordion, Card, Table} from "react-bootstrap";
import axios from "axios";

class AmateurQueries extends Component {
    state = {
    moviesReq5: [],
    moviesReq6: [], moviesReq7: [], moviesReq8: []

  }

  componentDidMount() {
    axios.get("/requete5")
      .then(res => {
        const moviesReq5 = res.data;
        this.setState({ moviesReq5});
      });
    axios.get("/requete4")
      .then(res => {
        const moviesReq6 = res.data;
        this.setState({ moviesReq6 });
      });
    axios.get("/requete7")
      .then(res => {
        const moviesReq7 = res.data;
        this.setState({ moviesReq7 });
      });
    axios.get("/requete8")
      .then(res => {
        const moviesReq8 = res.data;
        console.log(res.data);
        this.setState({ moviesReq8 });
      });

  }
    render(){
        return(
            <div className="main-container">
                <Navbar className="navbar">
                <div className="left-div-header">
                    <h1 className="nav-header">Ask Movies</h1>
                </div>
                    <div className="right-div-header">
                        <a className="no-underline">
                            <Link to="/" className="home-link" >
                                <h1 className="home-redirect">Home</h1>
                            </Link>
                        </a>
                    </div>
                </Navbar>
                <div className="main-body">
                    <h3 className="body-header">Discover Some Queries</h3>
                    <div>
                        <Accordion>
  <Card className="card-style">
    <Accordion.Toggle as={Card.Header} eventKey="0">
        <div>
        <h3 className="accordion-title">Most played role as a woman</h3>
    </div>
        </Accordion.Toggle>
    <Accordion.Collapse eventKey="0">
      <Card.Body>
          <div >
          <Table striped bordered hover variant="dark" className="table">
  <thead>
    <tr>
      <th className="table-title">Movie Name</th>
        <th className="table-title">Movie Number</th>
    </tr>
  </thead>
  <tbody className="scroll-body">
  { this.state.moviesReq5.map((movie) =><tr className="tr" ><th key={movie._id}>{movie._id}</th><th key={movie._id}>{movie.count}</th></tr>
        ) }
  </tbody>
</Table>
</div>
      </Card.Body>
    </Accordion.Collapse>
  </Card>
                            <br/>
<Card className="card-style">
    <Accordion.Toggle as={Card.Header} eventKey="1">
        <div>
        <h3 className="accordion-title">Best rated movie by more than 250 voters by decenny</h3>
    </div>
        </Accordion.Toggle>
    <Accordion.Collapse eventKey="1">
      <Card.Body>
          <div >
          <Table striped bordered hover variant="dark" className="table">
  <thead>
    <tr>
      <th className="table-title">Movie Name</th>
    </tr>
  </thead>
  <tbody className="scroll-body">
  { this.state.moviesReq6.map((movie,i) =><tr className="tr" ><th key={i}>{movie.movies_nb}</th></tr>
        ) }
  </tbody>
</Table>
</div>
      </Card.Body>
    </Accordion.Collapse>
  </Card>
                            <br/>
<Card className="card-style">
    <Accordion.Toggle as={Card.Header} eventKey="2">
        <div>
        <h3 className="accordion-title">Get most representative genre by keyword</h3>
    </div>
        </Accordion.Toggle>
    <Accordion.Collapse eventKey="2">
      <Card.Body>
         <div className="table-container">
          <Table striped bordered hover variant="dark" className="table">
  <thead>
    <tr>
      <th className="table-title">Movie Keyword</th>
    </tr>
  </thead>
  <tbody className="scroll-body">
  { this.state.moviesReq7.map((movie) =><tr className="tr" ><th key={movie["_id"]["keyword"]["id"]}>{movie["_id"]["keyword"]["name"]}</th></tr>
        ) }
  </tbody>
</Table>
</div>
      </Card.Body>
    </Accordion.Collapse>
  </Card>
                            <br/>
                            <Card className="card-style">
    <Accordion.Toggle as={Card.Header} eventKey="3">
        <div>
        <h3 className="accordion-title">Get cast names from the Top 10 Drama movies estimating the rate average</h3>
    </div>
        </Accordion.Toggle>
    <Accordion.Collapse eventKey="3">
      <Card.Body>
         <div className="table-container">
          <Table striped bordered hover variant="dark" className="table">
  <thead>
    <tr>
      <th className="table-title">Cast Name</th>
    </tr>
  </thead>
  <tbody className="scroll-body">
  { this.state.moviesReq8.map((movie, i) =><tr className="tr" ><th key={i}>{movie["_id"]["cast_name"]}</th></tr>
        ) }
  </tbody>
</Table>
</div>
      </Card.Body>
    </Accordion.Collapse>
  </Card>
</Accordion>
                    </div>
                </div>

            </div>
        );
    }
}

export default AmateurQueries;