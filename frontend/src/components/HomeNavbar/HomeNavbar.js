import { Navbar} from 'react-bootstrap';
import {Link} from 'react-router-dom';

import './HomeNavbar.css';

const HomeNavbar = ({tab}) => {
    return(
        <Navbar className="navbar justify-content-between w-100 pl-5">
            <div>
                <Link to='/'>
                    <h1 className="nav-header">Movies</h1>
                </Link>
            </div>
        </Navbar>
    )
}

export default HomeNavbar;